import React, { useState, useEffect } from 'react'
import { Chart, LineAdvance} from 'bizcharts';
import axios from 'axios'

import { Button } from '../Button'
import { Container, Image, Text } from './styles'


// 数据源
const data = [
	{
		month: "Jan",
		city: "Tokyo",
		temperature: 7
	},
	{
		month: "Jan",
		city: "London",
		temperature: 3.9
	},
	{
		month: "Feb",
		city: "Tokyo",
		temperature: 13
	},
	{
		month: "Feb",
		city: "London",
		temperature: 4.2
	},
	{
		month: "Mar",
		city: "Tokyo",
		temperature: 16.5
	},
	{
		month: "Mar",
		city: "London",
		temperature: 5.7
	},
	{
		month: "Apr",
		city: "Tokyo",
		temperature: 14.5
	},
	{
		month: "Apr",
		city: "London",
		temperature: 8.5
	},
	{
		month: "May",
		city: "Tokyo",
		temperature: 10
	},
	{
		month: "May",
		city: "London",
		temperature: 11.9
	},
	{
		month: "Jun",
		city: "Tokyo",
		temperature: 7.5
	},
	{
		month: "Jun",
		city: "London",
		temperature: 15.2
	},
	{
		month: "Jul",
		city: "Tokyo",
		temperature: 9.2
	},
	{
		month: "Jul",
		city: "London",
		temperature: 17
	},
	{
		month: "Aug",
		city: "Tokyo",
		temperature: 14.5
	},
	{
		month: "Aug",
		city: "London",
		temperature: 16.6
	},
	{
		month: "Sep",
		city: "Tokyo",
		temperature: 9.3
	},
	{
		month: "Sep",
		city: "London",
		temperature: 14.2
	},
	{
		month: "Oct",
		city: "Tokyo",
		temperature: 8.3
	},
	{
		month: "Oct",
		city: "London",
		temperature: 10.3
	},
	{
		month: "Nov",
		city: "Tokyo",
		temperature: 8.9
	},
	{
		month: "Nov",
		city: "London",
		temperature: 5.6
	},
	{
		month: "Dec",
		city: "Tokyo",
		temperature: 5.6
	},
	{
		month: "Dec",
		city: "London",
		temperature: 9.8
	}
];

export function Greetings() {
  const [cpuTemp, setCpuTemp] = useState(0)
  const [currentWeather, setCurrentWeather] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => {
      // Create an scoped async function in the hook
      async function getCPUTemp() {
        //console.log('doAction')
        setCpuTemp(await window.Main.doAction(''))
        // http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=3e391f6c15ae0e82f28bd2a6248d3efd
        
        axios
          .get(
            `http://api.openweathermap.org/data/2.5/forecast?id=2655603&appid=3e391f6c15ae0e82f28bd2a6248d3efd`
          )
          .then(res => {
            const weather = res.data
            console.log('weather temp', weather.list[0]);
            let temp = Math.round((weather.list[0].main.temp - 273.15) * 100) / 100
            setCurrentWeather(temp)
          })

      }
      // Execute the created function directly
      getCPUTemp()
    }, 5000)
  }, [])

  function handleSayHello() {
    window.Main.sendMessage('Hello World')
    console.log('Message sent! Check main process log in terminal.')
  }

  return (
    <Container>
      <Chart padding={[10, 20, 50, 40]} autoFit height={300} data={data} >
		<LineAdvance
			shape="smooth"
			point
			area
			position="month*temperature"
			color="city"
		/>
	
	</Chart>
      <Text>
        An Electron boilerplate including TypeScript, React, Jest and ESLint.
      </Text>
      <Button onClick={handleSayHello}>Send message to main process</Button>
      <br />
      {cpuTemp} CPU Temp
      <br/>
      Current Temperature in Birmingham GB {currentWeather} Celsius
    </Container>
  )
}

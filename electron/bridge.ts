import { contextBridge, ipcRenderer, ipcMain } from 'electron'
import si from 'systeminformation'

export const api = {
  /**
   * Here you can expose functions to the renderer process
   * so they can interact with the main (electron) side
   * without security problems.
   *
   * The function below can accessed using `window.Main.sayHello`
   */

  sendMessage: (message: string) => { 
    ipcRenderer.send('message', message)
  },
  doAction: async (arg:any) => {
    return await ipcRenderer.invoke('asynchronous-cpuinfo', arg);
},

  /**
   * Provide an easier way to listen to events
   */
  on: (channel: string, callback: Function) => {
    ipcRenderer.on(channel, (_, data) => callback(data))
  }
}

contextBridge.exposeInMainWorld('Main', api)

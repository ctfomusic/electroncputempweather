## Electron + TypeScript + React

Boilerplate for a project using Electron, React and Typescript.

Sorry I didn't get to style it. I would have added charts and timelines etc...

## Installation

Use a package manager of your choice (npm, yarn, etc.) in order to install all dependencies

```bash
yarn
```

## Usage
Make sure to run your terminal as Administrator in order for systeminormation to be accessible tot he app.

Just run `start` script.

```bash
yarn start
```

## Packaging

To generate the project package based on the OS you're running on, just run:

```bash
yarn package
```

## Contributing

Pull requests are always welcome 😃.

## License

[MIT](https://choosealicense.com/licenses/mit/)
